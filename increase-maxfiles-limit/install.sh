#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
    echo "Please run this script as root!"
    exit 1
fi

LAUNCH_DAEMONS_DIR="/Library/LaunchDaemons"
INCR_MAX_FILES_LIMIT_PLIST_SOURCE_PATH="./daemon/com.kuchkovsky.IncreaseMaxFilesLimit.plist"
INCR_MAX_FILES_LIMIT_PLIST_TARGET_PATH="$LAUNCH_DAEMONS_DIR/com.kuchkovsky.IncreaseMaxFilesLimit.plist"

echo "Installing increase-maxfiles-limit..."

cp "$INCR_MAX_FILES_LIMIT_PLIST_SOURCE_PATH" "$INCR_MAX_FILES_LIMIT_PLIST_TARGET_PATH"

echo "Starting the daemon..."
launchctl unload "$INCR_MAX_FILES_LIMIT_PLIST_TARGET_PATH" > /dev/null 2>&1
launchctl load "$INCR_MAX_FILES_LIMIT_PLIST_TARGET_PATH"

echo "Done"

#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
    echo "Please run this script as root!"
    exit 1
fi

LAUNCH_DAEMONS_DIR="/Library/LaunchDaemons"
INCR_MAX_FILES_LIMIT_PLIST_PATH="$LAUNCH_DAEMONS_DIR/com.kuchkovsky.IncreaseMaxFilesLimit.plist"

if ! [[ -f "$INCR_MAX_FILES_LIMIT_PLIST_PATH" ]]; then
  echo "increase-maxfiles-limit is not installed!"
  exit 1
fi

echo "Stopping the daemon..."
launchctl unload "$INCR_MAX_FILES_LIMIT_PLIST_PATH" > /dev/null 2>&1

echo "Uninstalling increase-maxfiles-limit..."
rm "$INCR_MAX_FILES_LIMIT_PLIST_PATH"

echo "Done"

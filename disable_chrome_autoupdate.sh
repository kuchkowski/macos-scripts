#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Please run this script as a regular user, not root!"
    exit 1
fi

echo "Disabling Chrome autoupdate..."

mkdir -p ~/Library/Application\ Support/Google/GoogleUpdater/
rm -rf ~/Library/Application\ Support/Google/GoogleUpdater/*
sudo chown nobody:nogroup ~/Library/Application\ Support/Google/GoogleUpdater/
sudo chmod 000 ~/Library/Application\ Support/Google/GoogleUpdater/

mkdir -p ~/Library/Google/GoogleSoftwareUpdate/
rm -rf ~/Library/Google/GoogleSoftwareUpdate/*
sudo chown nobody:nogroup ~/Library/Google/GoogleSoftwareUpdate/
sudo chmod 000 ~/Library/Google/GoogleSoftwareUpdate/

launchctl unload ~/Library/LaunchAgents/com.google.GoogleUpdater.wake.plist > /dev/null 2>&1
launchctl unload ~/Library/LaunchAgents/com.google.keystone.agent.plist > /dev/null 2>&1
launchctl unload ~/Library/LaunchAgents/com.google.keystone.xpcservice.plist > /dev/null 2>&1

rm -f ~/Library/LaunchAgents/com.google.GoogleUpdater.wake.plist
rm -f ~/Library/LaunchAgents/com.google.keystone.agent.plist
rm -f ~/Library/LaunchAgents/com.google.keystone.xpcservice.plist

echo "Done"

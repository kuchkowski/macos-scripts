#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Please run this script as a regular user, not root!"
    exit 1
fi

SCRIPTS_DIR="$HOME/Library/Scripts/com.kuchkovsky"
LAUNCH_AGENTS_DIR="$HOME/Library/LaunchAgents"
DISMISS_RELAY_NOTIFICATIONS_SCPT_PATH="$SCRIPTS_DIR/dismiss_private_relay_notifications.scpt"
KEEP_AWAKE_ON_VPN_SH_PATH="$SCRIPTS_DIR/keep_awake_on_vpn.sh"
KEEP_AWAKE_ON_VPN_PLIST_PATH="$LAUNCH_AGENTS_DIR/com.kuchkovsky.KeepAwakeOnVPN.plist"

if ! [[ -f "$KEEP_AWAKE_ON_VPN_PLIST_PATH" ]]; then
  echo "keep-awake-on-vpn is not installed!"
  exit 1
fi

echo "Stopping the daemon..."
launchctl unload "$KEEP_AWAKE_ON_VPN_PLIST_PATH" > /dev/null 2>&1

echo "Uninstalling keep-awake-on-vpn..."
rm "$KEEP_AWAKE_ON_VPN_PLIST_PATH"
rm "$KEEP_AWAKE_ON_VPN_SH_PATH"
rm "$DISMISS_RELAY_NOTIFICATIONS_SCPT_PATH"

echo "Done"

#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Please run this script as a regular user, not root!"
    exit 1
fi

if ! [[ -d '/Applications/Lungo.app' ]]; then
  echo "Lungo is not installed! Please install it before installing this daemon"
  exit 1
fi

SCRIPTS_DIR="$HOME/Library/Scripts/com.kuchkovsky"
LAUNCH_AGENTS_DIR="$HOME/Library/LaunchAgents"
DISMISS_RELAY_NOTIFICATIONS_SCPT_SOURCE_PATH="./daemon/dismiss_private_relay_notifications.scpt"
KEEP_AWAKE_ON_VPN_SH_SOURCE_PATH="./daemon/keep_awake_on_vpn.sh"
KEEP_AWAKE_ON_VPN_PLIST_SOURCE_PATH="./daemon/com.kuchkovsky.KeepAwakeOnVPN.plist.tmpl"
DISMISS_RELAY_NOTIFICATIONS_SCPT_TARGET_PATH="$SCRIPTS_DIR/dismiss_private_relay_notifications.scpt"
KEEP_AWAKE_ON_VPN_SH_TARGET_PATH="$SCRIPTS_DIR/keep_awake_on_vpn.sh"
KEEP_AWAKE_ON_VPN_PLIST_TARGET_PATH="$LAUNCH_AGENTS_DIR/com.kuchkovsky.KeepAwakeOnVPN.plist"

echo "Installing keep-awake-on-vpn..."

mkdir -p "$SCRIPTS_DIR"

cp "$DISMISS_RELAY_NOTIFICATIONS_SCPT_SOURCE_PATH" "$DISMISS_RELAY_NOTIFICATIONS_SCPT_TARGET_PATH"
cp "$KEEP_AWAKE_ON_VPN_SH_SOURCE_PATH" "$KEEP_AWAKE_ON_VPN_SH_TARGET_PATH"

ESCAPED_KEEP_AWAKE_ON_VPN_SH_PATH=$(printf '%s\n' "$KEEP_AWAKE_ON_VPN_SH_TARGET_PATH" | sed -e 's/[\/&]/\\&/g')
sed "s/{KEEP_AWAKE_ON_VPN_SH_PATH}/${ESCAPED_KEEP_AWAKE_ON_VPN_SH_PATH}/g" "$KEEP_AWAKE_ON_VPN_PLIST_SOURCE_PATH" \
    > "$KEEP_AWAKE_ON_VPN_PLIST_TARGET_PATH"

echo "Starting the daemon..."
launchctl unload "$KEEP_AWAKE_ON_VPN_PLIST_TARGET_PATH" > /dev/null 2>&1
launchctl load "$KEEP_AWAKE_ON_VPN_PLIST_TARGET_PATH"

echo "Done"

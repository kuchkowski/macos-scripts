tell application "System Events"
    try
        -- Assuming 'Notification Center' window can be targeted directly
        set _groups to groups of UI element 1 of scroll area 1 of group 1 of window "Notification Center" of application process "NotificationCenter"
        
        repeat with _group in _groups
            -- Attempt to find a UI element that represents the notification's title or content
            set _titles to static texts of _group
            set _found to false
            repeat with _title in _titles
                if value of _title contains "Private Relay" then
                    set _found to true
                    exit repeat
                end if
            end repeat
            
            if _found then
                set _actions to actions of _group
                repeat with _action in _actions
                    if description of _action is "Close" then
                        perform _action
                        exit repeat
                    end if
                end repeat
            end if
        end repeat
        
    end try
end tell

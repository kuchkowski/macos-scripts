#!/bin/bash

if ! [[ -d '/Applications/Lungo.app' ]]; then
  echo "Lungo is not installed! Please install it to run this daemon"
  exit 1
fi

__DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

VPN_PRIVATE_IP_PATTERN='10\.11\.'
IS_ACTIVATED=false
IS_RELAY_UNAVAILABLE_MSG_DISMISSED=false
IS_RELAY_ACTIVE_MSG_DISMISSED=false

while true; do
	if ifconfig | grep -q $VPN_PRIVATE_IP_PATTERN; then
		echo "VPN is running"
	
		# Always activate Lungo if VPN is running, even if user decides to deactivate it manually
		if ! pmset -g assertions | grep -q Lungo; then
			echo "Activating Lungo..."
			open --background 'lungo:activate'
			IS_ACTIVATED=true
		fi
		
		if [[ "$IS_ACTIVATED" = true ]]; then
			echo "Lungo is activated by KeepAwakeOnVPN"
		else
			echo "Lungo is activated by the user"
		fi
		
		# Cisco VPN connection breaks iCloud Private Relay.
		# In turn, Relay sends a corresponding notification that requires a manual action from the user.
		# This script dismisses the notification without the user's interaction
		if [[ "$IS_RELAY_UNAVAILABLE_MSG_DISMISSED" = false ]]; then
		    osascript "${__DIR}/dismiss_private_relay_notifications.scpt"
			IS_RELAY_UNAVAILABLE_MSG_DISMISSED=true
			IS_RELAY_ACTIVE_MSG_DISMISSED=false
			
			echo "Sent dismiss command to 'Private Relay Unavailable' notification"
		fi
	else
		echo "VPN is not running"
	
		# Deactivate Lungo only if it was activated by KeepAwakeOnVPN.
		# Don't touch it if it was activated by the user
		if [[ "$IS_ACTIVATED" = true ]] && pmset -g assertions | grep -q Lungo; then
			echo "Deactivating Lungo..."
			open --background 'lungo:deactivate'
			IS_ACTIVATED=false
		fi
	
		echo "Lungo is not activated by KeepAwakeOnVPN"
		
		if [[ "$IS_RELAY_ACTIVE_MSG_DISMISSED" = false ]]; then
		    osascript "${__DIR}/dismiss_private_relay_notifications.scpt"
			IS_RELAY_UNAVAILABLE_MSG_DISMISSED=false
			IS_RELAY_ACTIVE_MSG_DISMISSED=true
			
			echo "Sent dismiss command to 'Private Relay is Active' notification"
		fi
	fi
	
	echo
	sleep 30
done

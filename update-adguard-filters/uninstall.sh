#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Please run this script as a regular user, not root!"
    exit 1
fi

SCRIPTS_DIR="$HOME/Library/Scripts/com.kuchkovsky"
LAUNCH_AGENTS_DIR="$HOME/Library/LaunchAgents"
UPD_ADG_FILTERS_SH_PATH="$SCRIPTS_DIR/update_adguard_filters.sh"
UPD_ADG_FILTERS_PLIST_PATH="$LAUNCH_AGENTS_DIR/com.kuchkovsky.UpdateAdGuardFilters.plist"

if ! [[ -f "$UPD_ADG_FILTERS_PLIST_PATH" ]]; then
  echo "update-adguard-filters is not installed!"
  exit 1
fi

echo "Stopping the daemon..."
launchctl unload "$UPD_ADG_FILTERS_PLIST_PATH" > /dev/null 2>&1

echo "Uninstalling update-adguard-filters..."
rm "$UPD_ADG_FILTERS_PLIST_PATH"
rm "$UPD_ADG_FILTERS_SH_PATH"

echo "Done"

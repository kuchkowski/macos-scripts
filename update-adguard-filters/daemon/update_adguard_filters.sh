#!/bin/bash

if ! [[ -d '/Applications/AdGuard for Safari.app' ]]; then
  echo "AdGuard for Safari is not installed! Please install it to run this daemon"
  exit 1
fi


echo "Clearing the clipboard before launching AdGuard for Safari..."
pbcopy < /dev/null

echo "Starting AdGuard for Safari in background..."
open --background -a "AdGuard for Safari"

echo "Waiting 7 minutes for AdGuard to update its filters..."
sleep 420

echo "Quitting AdGuard for Safari..."
osascript -e 'quit app "AdGuard for Safari"'

echo "Done"

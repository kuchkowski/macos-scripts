#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Please run this script as a regular user, not root!"
    exit 1
fi

if ! [[ -d '/Applications/AdGuard for Safari.app' ]]; then
  echo "AdGuard for Safari is not installed! Please install it before installing this daemon"
  exit 1
fi

SCRIPTS_DIR="$HOME/Library/Scripts/com.kuchkovsky"
LAUNCH_AGENTS_DIR="$HOME/Library/LaunchAgents"
UPD_ADG_FILTERS_SH_SOURCE_PATH="./daemon/update_adguard_filters.sh"
UPD_ADG_FILTERS_PLIST_SOURCE_PATH="./daemon/com.kuchkovsky.UpdateAdGuardFilters.plist.tmpl"
UPD_ADG_FILTERS_SH_TARGET_PATH="$SCRIPTS_DIR/update_adguard_filters.sh"
UPD_ADG_FILTERS_PLIST_TARGET_PATH="$LAUNCH_AGENTS_DIR/com.kuchkovsky.UpdateAdGuardFilters.plist"

echo "Installing update-adguard-filters..."

mkdir -p "$SCRIPTS_DIR"

cp "$UPD_ADG_FILTERS_SH_SOURCE_PATH" "$UPD_ADG_FILTERS_SH_TARGET_PATH"

ESCAPED_UPD_ADG_FILTERS_SH_PATH=$(printf '%s\n' "$UPD_ADG_FILTERS_SH_TARGET_PATH" | sed -e 's/[\/&]/\\&/g')
sed "s/{UPD_ADG_FILTERS_SH_PATH}/${ESCAPED_UPD_ADG_FILTERS_SH_PATH}/g" "$UPD_ADG_FILTERS_PLIST_SOURCE_PATH" \
    > "$UPD_ADG_FILTERS_PLIST_TARGET_PATH"

echo "Starting the daemon..."
launchctl unload "$UPD_ADG_FILTERS_PLIST_TARGET_PATH" > /dev/null 2>&1
launchctl load "$UPD_ADG_FILTERS_PLIST_TARGET_PATH"

echo "Done"

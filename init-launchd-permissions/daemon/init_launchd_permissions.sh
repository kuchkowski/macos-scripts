#!/bin/bash

echo "Launching Automator..."
open -a "Automator"

echo "Waiting 2 seconds..."
sleep 2

echo "Hiding Automator window..."
osascript -e 'tell application "System Events" to set visible of process "Automator" to false'

echo "Waiting 1 second..."
sleep 1

echo "Quitting automator..."
osascript -e 'quit app "Automator"'

echo "Done"

#!/bin/bash

SCRIPTS_DIR="$HOME/Library/Scripts/com.kuchkovsky"
LAUNCH_AGENTS_DIR="$HOME/Library/LaunchAgents"
INIT_LAUNCHD_PERMISSIONS_SH_SOURCE_PATH="./daemon/init_launchd_permissions.sh"
INIT_LAUNCHD_PERMISSIONS_PLIST_SOURCE_PATH="./daemon/com.kuchkovsky.InitLaunchdPermissions.plist.tmpl"
INIT_LAUNCHD_PERMISSIONS_SH_TARGET_PATH="$SCRIPTS_DIR/init_launchd_permissions.sh"
INIT_LAUNCHD_PERMISSIONS_PLIST_TARGET_PATH="$LAUNCH_AGENTS_DIR/com.kuchkovsky.InitLaunchdPermissions.plist"

echo "This scripts inits the permissions that are needed for other daemons in this repo"
echo "Please confirm all permission popups that you will see"

read -r -p "When you are ready, press any key to continue..." -n1 -s && echo

echo "Installing and starting the service..."

mkdir -p "$SCRIPTS_DIR"
cp "$INIT_LAUNCHD_PERMISSIONS_SH_SOURCE_PATH" "$INIT_LAUNCHD_PERMISSIONS_SH_TARGET_PATH"

ESCAPED_INIT_LAUNCHD_PERMISSIONS_SH_PATH=$(printf '%s\n' "$INIT_LAUNCHD_PERMISSIONS_SH_TARGET_PATH" | sed -e 's/[\/&]/\\&/g')
sed "s/{INIT_LAUNCHD_PERMISSIONS_SH_PATH}/${ESCAPED_INIT_LAUNCHD_PERMISSIONS_SH_PATH}/g" "$INIT_LAUNCHD_PERMISSIONS_PLIST_SOURCE_PATH" \
    > "$INIT_LAUNCHD_PERMISSIONS_PLIST_TARGET_PATH"

launchctl unload "$INIT_LAUNCHD_PERMISSIONS_PLIST_TARGET_PATH" > /dev/null 2>&1
launchctl load "$INIT_LAUNCHD_PERMISSIONS_PLIST_TARGET_PATH"

read -r -p "If you confirmed all the popups or there are no popups available, press any key to continue..." -n1 -s && echo

echo "Stopping and uninstalling the service..."

launchctl unload "$INIT_LAUNCHD_PERMISSIONS_PLIST_TARGET_PATH"
rm "$INIT_LAUNCHD_PERMISSIONS_PLIST_TARGET_PATH"
rm "$INIT_LAUNCHD_PERMISSIONS_SH_TARGET_PATH"

echo "Done"

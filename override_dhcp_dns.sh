#!/bin/bash

while read -r line; do
    S_NAME=$(echo "$line" | awk -F  "(, )|(: )|[)]" '{print $2}')
    S_DEV=$(echo "$line" | awk -F  "(, )|(: )|[)]" '{print $4}')
    
    if [ -n "$S_DEV" ]; then
        IF_OUT="$(ifconfig "$S_DEV" 2>/dev/null)"
        echo "$IF_OUT" | grep 'status: active' > /dev/null 2>&1
        rc="$?"
        
        if [ "$rc" -eq 0 ]; then
            NETWORK_SERVICE="$S_NAME"
            break
        fi
    fi
done <<< "$(networksetup -listnetworkserviceorder | grep 'Hardware Port')"

if [ -z "$NETWORK_SERVICE" ]; then
    >&2 echo "ERROR: could not find any active network service"
    exit 1
fi

if networksetup -getdnsservers "$NETWORK_SERVICE" | grep -q "There aren't any DNS Servers"; then
	echo "Overriding DHCP DNS on $NETWORK_SERVICE..."
	networksetup -setdnsservers "$NETWORK_SERVICE" 1.1.1.1 1.0.0.1
	echo "DHCP DNS is disabled"
else
	echo "Restoring DHCP DNS on $NETWORK_SERVICE..."
	networksetup -setdnsservers "$NETWORK_SERVICE" empty
	echo "DHCP DNS is enabled"
fi

echo "Flushing DNS cache..."
sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder
echo "Done"

#!/bin/bash

function restart_network_interfaces()
{
  net_interfaces=("$@")
  for net_interface in "${net_interfaces[@]}"; do
    echo "Restarting interface: ${net_interface}"
    sudo ifconfig "${net_interface}" down
    sudo ifconfig "${net_interface}" up
    ifconfig -u "${net_interface}"
  done

}

CISCO_VPN_AGENT="/opt/cisco/anyconnect/bin/vpnagentd"
CISCO_VPN_AGENT_CMD="$CISCO_VPN_AGENT -execv_instance"

if [[ -f $CISCO_VPN_AGENT ]]; then
    echo "Restarting ${CISCO_VPN_AGENT_CMD}..."
    sudo pkill -9 -f "${CISCO_VPN_AGENT_CMD}"
fi


echo "Restarting networks interfaces..."

ETHERNET_INT=$(networksetup -listnetworkserviceorder | grep '^.*(\d).*$' -A 1 | sed -En 's|^\(Hardware Port: .*USB.*LAN.*, Device: (en.)\)$|\1|p')
restart_network_interfaces "${ETHERNET_INT[@]}"

WIFI_INT=$(networksetup -listnetworkserviceorder | sed -En 's/^\(Hardware Port: (Wi-Fi|AirPort), Device: (en.)\)$/\2/p')
restart_network_interfaces "${WIFI_INT[@]}"

echo "Done"

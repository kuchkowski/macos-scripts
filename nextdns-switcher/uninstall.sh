#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
    echo "Please run this script as root!"
    exit 1
fi

SCRIPTS_DIR="/Library/Scripts/com.kuchkovsky"
LAUNCH_DAEMONS_DIR="/Library/LaunchDaemons"
NEXTDNS_SWITCHER_SH_PATH="$SCRIPTS_DIR/nextdns_switcher.sh"
NEXTDNS_SWITCHER_PLIST_PATH="$LAUNCH_DAEMONS_DIR/com.kuchkovsky.NextDNSSwitcher.plist"

if ! [[ -f "$NEXTDNS_SWITCHER_PLIST_PATH" ]]; then
  echo "nextdns-switcher is not installed!"
  exit 1
fi

echo "Stopping the daemon..."
launchctl unload "$NEXTDNS_SWITCHER_PLIST_PATH" > /dev/null 2>&1

echo "Uninstalling nextdns-switcher..."
rm "$NEXTDNS_SWITCHER_PLIST_PATH"
rm "$NEXTDNS_SWITCHER_SH_PATH"

echo "Done"

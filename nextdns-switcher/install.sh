#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
    echo "Please run this script as root!"
    exit 1
fi


NEXTDNS="/opt/homebrew/bin/nextdns"

if ! [[ -f $NEXTDNS ]] || $NEXTDNS status | grep -q 'not installed'; then
  echo "NextDNS CLI Client is not installed! Please install it before installing this daemon"
  exit 1
fi


WIFI_WHITELIST_FILE="wifi_whitelist.txt"

if ! [[ -s $WIFI_WHITELIST_FILE ]]; then
    echo "Please add your whitelisted Wi-Fi networks to $WIFI_WHITELIST_FILE line by line, and run the installer again"

    touch $WIFI_WHITELIST_FILE
    chmod 777 $WIFI_WHITELIST_FILE
    exit 1
fi


SCRIPTS_DIR="/Library/Scripts/com.kuchkovsky"
LAUNCH_DAEMONS_DIR="/Library/LaunchDaemons"
NEXTDNS_SWITCHER_SH_SOURCE_PATH="./daemon/nextdns_switcher.sh.tmpl"
NEXTDNS_SWITCHER_PLIST_SOURCE_PATH="./daemon/com.kuchkovsky.NextDNSSwitcher.plist.tmpl"
NEXTDNS_SWITCHER_SH_TARGET_PATH="$SCRIPTS_DIR/nextdns_switcher.sh"
NEXTDNS_SWITCHER_PLIST_TARGET_PATH="$LAUNCH_DAEMONS_DIR/com.kuchkovsky.NextDNSSwitcher.plist"

echo "Installing nextdns-switcher..."

mkdir -p "$SCRIPTS_DIR"

WIFI_WHITELIST=$(sed 's/\(.*\)/"\1"/g' $WIFI_WHITELIST_FILE | tr '\n' ' ' | awk '{$1=$1};1')

sed "s/'{WIFI_WHITELIST}'/${WIFI_WHITELIST}/g" "$NEXTDNS_SWITCHER_SH_SOURCE_PATH" \
    > "$NEXTDNS_SWITCHER_SH_TARGET_PATH"
chmod +x "$NEXTDNS_SWITCHER_SH_TARGET_PATH"

ESCAPED_NEXTDNS_SWITCHER_SH_PATH=$(printf '%s\n' "$NEXTDNS_SWITCHER_SH_TARGET_PATH" | sed -e 's/[\/&]/\\&/g')
sed "s/{NEXTDNS_SWITCHER_SH_PATH}/${ESCAPED_NEXTDNS_SWITCHER_SH_PATH}/g" "$NEXTDNS_SWITCHER_PLIST_SOURCE_PATH" \
    > "$NEXTDNS_SWITCHER_PLIST_TARGET_PATH"

echo "Starting the daemon..."
launchctl unload "$NEXTDNS_SWITCHER_PLIST_TARGET_PATH" > /dev/null 2>&1
launchctl load "$NEXTDNS_SWITCHER_PLIST_TARGET_PATH"

echo "Done"
